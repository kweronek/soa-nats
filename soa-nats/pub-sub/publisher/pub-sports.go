package main

import (
	"github.com/nats-io/nats.go"
	"log"
)

func main() {

	nc, err := nats.Connect(nats.DefaultURL) //Connect to nats server
	if err != nil {
		log.Fatal(err)
	}
	defer nc.Close()

	nc.Publish("FUAS-news.sports", []byte("Dies ist eine FUAS-news Sport-Nachricht"))
}
