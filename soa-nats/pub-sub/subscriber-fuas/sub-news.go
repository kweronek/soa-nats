package main

import (
	"fmt"
	"github.com/nats-io/nats.go"
	"log"
	"runtime"
)

// NATS-publish-subcribe: subscribe
func main() {

	nc, err := nats.Connect(nats.DefaultURL)
	if err != nil {
		log.Fatal(err)
	}
	defer nc.Close()

	subject := "FUAS-news.*"
	fmt.Printf("Start Subscription for %s\n", subject)

	nc.Subscribe(subject, func(m *nats.Msg) {
		fmt.Printf("%s\n", m.Data)
	})

	// to keep connection alive:
	runtime.Goexit()
}
