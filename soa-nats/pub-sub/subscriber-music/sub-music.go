package main

import (
	"fmt"
	"github.com/nats-io/nats.go"
	"log"
	//	"sync"
	"runtime"
)

func main() {

	nc, err := nats.Connect(nats.DefaultURL)
	if err != nil {
		log.Fatal(err)
	}
	defer nc.Close()

	subject := "FUAS-news.music"
	fmt.Printf("Subscription for %s\n", subject)

	nc.Subscribe(subject, func(m *nats.Msg) {
		fmt.Printf("%s\n", m.Data)
	})

	//)
	//	b := sync.WaitGroup{}
	//	b.Add(2)
	//	b.Wait()
	runtime.Goexit()

}
