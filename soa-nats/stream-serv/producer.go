package main

import (
	"log"
	"strconv"
	"time"

	stan "github.com/nats-io/stan.go"
)

func main() {

	sc, err := stan.Connect("prod","simple-pub")
	if err != nil{
		log.Fatal(err)
	}
	defer sc.Close()

	for i := 1; ; i++ {
		sc.Publish("Order", []byte("Order "+strconv.Itoa(i)))
		time.Sleep(2 * time.Second)

	}
	
}
