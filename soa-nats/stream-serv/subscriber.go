package main

import (
	"fmt"
	"log"
	"sync"

	stan "github.com/nats-io/stan.go"
)

func main() {

	sc, err := stan.Connect("prod", "sub-1")
	if err != nil{
		log.Fatal(err)
	}
	defer sc.Close()

	sc.Subscribe("Order", func(m *stan.Msg) {
		fmt.Printf("Got: %s\n", string(m.Data))
	})

	b := sync.WaitGroup{}
	b.Add(2)
	b.Wait()

}
