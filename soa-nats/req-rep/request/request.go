package main

import (
	"fmt"
	"github.com/nats-io/nats.go"
	"log"
	"strconv"
	"time"
)
func main() {
	nc, _ := nats.Connect(nats.DefaultURL)
	defer nc.Close()
	for i := 100000000000000000; i < 1000000000000000000; i++ {
		res, err := nc.Request("services", []byte(strconv.Itoa(i)), 10*time.Second);
		if err != nil {
			log.Print(err)
			continue
		}
		fmt.Print(i," ",string(res.Data), "\n")

	}
}